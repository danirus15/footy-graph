<html>
<?php include "includes/head_pop.php";?>
<link href='css/preview.css' rel='stylesheet' type='text/css'>
<body >
<!-- <div class="link_formasi_css">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css'>
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim1">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim2">
</div> -->
<div class="pop_bg"></div>
<div class="pop_container pop_container650">
	<div class="pd20 bg_white" style="position: relative">
    	<span class="close_box_in close_box_style">x</span>
    	<div class="title_pop">Preview</div>
    	<br>
    	<div class="p640">
			<div class="formasi_area ">
			<div class="pitch">
				<div class="tboard">
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
					</div>
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="line" id="player_container">
					<div id="pitch_pattern" class="pitch2"></div>
					<div class="tim tim1" style="width: 50%; height: 100%;">
						<div id="tf1">
														<div class="player player_prev" id="1a" style="left: 8%; top: 45%;">
								<div class="bullet"><span id="pno1a">1</span></div>
								<strong id="pname1a">Rui PatrÃ­cio</strong>
							</div>
														<div class="player player_prev" id="2a" style="left: 26%; top: 31%;">
								<div class="bullet"><span id="pno2a">3</span></div>
								<strong id="pname2a">Pepe</strong>
								<div class="ico">
									<img src="img/ico-yellow.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="3a" style="left: 30%; top: 6%;">
								<div class="bullet"><span id="pno3a">5</span></div>
								<strong id="pname3a">Raphael Guerreiro</strong>
								<div class="ico">
									<img src="img/ico-red.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="4a" style="left: 26%; top: 60%;">
								<div class="bullet"><span id="pno4a">6</span></div>
								<strong id="pname4a">R. Carvalho</strong>
								<div class="ico">
									<img src="img/ico-red-yellow.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="5a" style="left: 81%; top: 31%;">
								<div class="bullet"><span id="pno5a">7</span></div>
								<strong id="pname5a">Ronaldo</strong>
								<div class="ico">
									<img src="img/ico-up.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="6a" style="left: 65%; top: 45%;">
								<div class="bullet"><span id="pno6a">8</span></div>
								<strong id="pname6a">JoÃ£o Moutinho</strong>
								<div class="ico">
									<img src="img/ico-down.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="7a" style="left: 49%; top: 67%;">
								<div class="bullet"><span id="pno7a">10</span></div>
								<strong id="pname7a">JoÃ£o MÃ¡rio</strong>
								<div class="ico">
									<img src="img/ico-capt.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="8a" style="left: 30%; top: 82%;">
								<div class="bullet"><span id="pno8a">11</span></div>
								<strong id="pname8a">Vieirinha</strong>
								<div class="ico">
									<img src="img/ico-goal.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="9a" style="left: 47%; top: 45%;">
								<div class="bullet"><span id="pno9a">13</span></div>
								<strong id="pname9a">Danilo</strong>
								<div class="ico">
									<img src="img/ico-assist.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="10a" style="left: 49%; top: 24%;">
								<div class="bullet"><span id="pno10a">15</span></div>
								<strong id="pname10a">AndrÃ© Gomes</strong>
								<div class="ico">
									<img src="img/ico-goal.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="11a" style="left: 81%; top: 60%;">
								<div class="bullet"><span id="pno11a">17</span></div>
								<strong id="pname11a">Nani</strong>
							</div>
							
														<div class="view_player pn1a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/93443.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/93443.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Rui PatrÃ­cio</h3>
										<h4>Goalkeeper</h4>
										<h5>Sporting CP (POR)</h5>
										<h6>15/02/1988</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn2a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/95417.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/95417.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Pepe</h3>
										<h4>Defender</h4>
										<h5>Real Madrid (ESP)</h5>
										<h6>26/02/1983</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn3a" style="left: 105.5px; top: -46.9062px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250066156.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250066156.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Raphael Guerreiro</h3>
										<h4>Defender</h4>
										<h5>Lorient (FRA)</h5>
										<h6>22/12/1993</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn4a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/31429.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/31429.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>R. Carvalho</h3>
										<h4>Defender</h4>
										<h5>Monaco (FRA)</h5>
										<h6>18/05/1978</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn5a" style="left: 276.344px; top: 49.3438px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/63706.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/63706.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Ronaldo</h3>
										<h4>Forward</h4>
										<h5>Real Madrid (ESP)</h5>
										<h6>05/02/1985</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn6a" style="left: 222.75px; top: 103.25px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/70098.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/70098.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>JoÃ£o Moutinho</h3>
										<h4>Midfield</h4>
										<h5>Monaco (FRA)</h5>
										<h6>08/09/1986</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn7a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250014109.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250014109.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>JoÃ£o MÃ¡rio</h3>
										<h4>Midfield</h4>
										<h5>Sporting CP (POR)</h5>
										<h6>19/01/1993</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn8a" style="left: 105.5px; top: 245.688px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/70099.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/70099.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Vieirinha</h3>
										<h4>Midfield</h4>
										<h5>Wolfsburg (GER)</h5>
										<h6>24/01/1986</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn9a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250014528.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250014528.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Danilo</h3>
										<h4>Midfield</h4>
										<h5>Porto (POR)</h5>
										<h6>09/09/1991</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn10a" style="left: 169.141px; top: 22.3906px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250017709.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250017709.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>AndrÃ© Gomes</h3>
										<h4>Midfield</h4>
										<h5>Valencia (ESP)</h5>
										<h6>30/07/1993</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn11a" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/101336.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/101336.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Nani</h3>
										<h4>Forward</h4>
										<h5>Fenerbahçe (TUR)</h5>
										<h6>17/11/1986</h6>
									</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
						
					</div>
					<div class="tim tim2" style="width: 50%; height: 100%;left: 50%;">
						<div id="tf2">
														<div class="player player_prev" id="1b" style="right: 8%; top: 45%;">
								<div class="bullet"><span id="pno1b">1</span></div>
								<strong id="pname1b">HalldÃ³rsson</strong>
							</div>
														<div class="player player_prev" id="2b" style="right: 30%; top: 8%;">
								<div class="bullet"><span id="pno2b">2</span></div>
								<strong id="pname2b">B. SÃ¦varsson</strong>
							</div>
														<div class="player player_prev" id="3b" style="right: 26%; top: 30%;">
								<div class="bullet"><span id="pno3b">6</span></div>
								<strong id="pname3b">R. Sigurdsson</strong>
							</div>
														<div class="player player_prev" id="4b" style="right: 63%; top: 8%;">
								<div class="bullet"><span id="pno4b">7</span></div>
								<strong id="pname4b">Gudmundsson</strong>
							</div>
														<div class="player player_prev" id="5b" style="right: 63%; top: 84%;">
								<div class="bullet"><span id="pno5b">8</span></div>
								<strong id="pname5b">B. Bjarnason</strong>
							</div>
														<div class="player player_prev" id="6b" style="right: 81%; top: 30%;">
								<div class="bullet"><span id="pno6b">9</span></div>
								<strong id="pname6b">SigthÃ³rsson</strong>
								<div class="ico">
									<img src="img/ico-up.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="7b" style="right: 47%; top: 59%;">
								<div class="bullet"><span id="pno7b">10</span></div>
								<strong id="pname7b">G. Sigurdsson</strong>
							</div>
														<div class="player player_prev" id="8b" style="right: 26%; top: 59%;">
								<div class="bullet"><span id="pno8b">14</span></div>
								<strong id="pname8b">Ãrnason</strong>
								<div class="ico">
									<img src="img/ico-up.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="9b" style="right: 81%; top: 59%;">
								<div class="bullet"><span id="pno9b">15</span></div>
								<strong id="pname9b">BÃ¶dvarsson</strong>
							</div>
														<div class="player player_prev" id="10b" style="right: 47%; top: 30%;">
								<div class="bullet"><span id="pno10b">17</span></div>
								<strong id="pname10b">A. Gunnarsson</strong>
								<div class="ico">
									<img src="img/ico-up.png" alt="">
								</div>
							</div>
														<div class="player player_prev" id="11b" style="right: 30%; top: 84%;">
								<div class="bullet"><span id="pno11b">23</span></div>
								<strong id="pname11b">A. SkÃºlason</strong>
							</div>
							
														<div class="view_player pn1b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/106184.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/106184.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>HalldÃ³rsson</h3>
										<h4>Goalkeeper</h4>
										<h5>Bodø/Glimt (NOR)</h5>
										<h6>27/04/1984</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn2b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/104623.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/104623.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>B. SÃ¦varsson</h3>
										<h4>Defender</h4>
										<h5>Hammarby (SWE)</h5>
										<h6>11/11/1984</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn3b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/65714.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/65714.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>R. Sigurdsson</h3>
										<h4>Defender</h4>
										<h5>Krasnodar (RUS)</h5>
										<h6>19/06/1986</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn4b" style="left: 78.9531px; top: -39.2031px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250003458.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250003458.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Gudmundsson</h3>
										<h4>Forward</h4>
										<h5>Charlton (ENG)</h5>
										<h6>27/10/1990</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn5b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/97142.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/97142.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>B. Bjarnason</h3>
										<h4>Midfield</h4>
										<h5>Basel (SUI)</h5>
										<h6>27/05/1988</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn6b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/107782.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/107782.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>SigthÃ³rsson</h3>
										<h4>Forward</h4>
										<h5>Nantes (FRA)</h5>
										<h6>14/03/1990</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn7b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/102293.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/102293.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>G. Sigurdsson</h3>
										<h4>Midfield</h4>
										<h5>Swansea (ENG)</h5>
										<h6>08/09/1989</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn8b" style="left: 202.906px; top: 157.141px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/98970.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/98970.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>Ãrnason</h3>
										<h4>Midfield</h4>
										<h5>Malmö (SWE)</h5>
										<h6>13/10/1982</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn9b" style="left: 18.6562px; top: 157.141px; display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/250024873.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/250024873.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>BÃ¶dvarsson</h3>
										<h4>Forward</h4>
										<h5>Kaiserslautern (GER)</h5>
										<h6>25/05/1992</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn10b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/102287.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/102287.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>A. Gunnarsson</h3>
										<h4>Midfield</h4>
										<h5>Cardiff (WAL)</h5>
										<h6>22/04/1989</h6>
									</div>
								<div class="clearfix"></div>
							</div>
														<div class="view_player pn11b" style="display: none;">
								<div class="pic lqd imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;http://api.footygraph.com/photo/73335.jpg&quot;); background-size: cover; background-position: 50% 0%; background-repeat: no-repeat;">
									<img src="http://api.footygraph.com/photo/73335.jpg" alt="" style="display: none;"></div>
									<div class="text">
										<h3>A. SkÃºlason</h3>
										<h4>Defender</h4>
										<h5>OB (DEN)</h5>
										<h6>14/05/1987</h6>
									</div>
								<div class="clearfix"></div>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="clearfix"></div>
				<div class="fl w50p">
					<h4>4-4-2</h4>
					<h2>Manchester United</h2>
					<h5>Louis van gaal</h5>
				</div>
				<div class="fr w50p" align="right">
					<h4>4-4-2</h4>
					<h2>Chelsea fc</h2>
					<h5>jose mourinho</h5>
				</div>
				<div class="clearfix"></div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix pt10"></div>
				
				</form>
			</div>
		</div> 
		<div class="clearfix"></div>
		</div>
		<div class="formasi embed_setting">
		<div class="clearfix"></div>

		<div class="embed_info">
			<div class="notif_copy" style="display:none;">Embed Copied</div>
			<div>
				<input type="text" class="input100" id="dynamic" value="www.footygraph.com/12903923" disabled="disabled">
				<a id="copy-dynamic" class="btn_create2">Copy Embed</a>
				<!-- <input type="button" value="Copy Embed" class="btn_create2"> -->

			</div>
			
		</div>
		<div class="clearfix"></div>
	</div>
    </div>
</div>

</body>
<?php include "includes/js.php";?>
<script src="js/jquery.zclip.min.js"></script>
<script>
	$(document).ready(function(){
	    $('a#copy-dynamic').zclip({
	        path:'js/ZeroClipboard.swf',
	        afterCopy:function(){
				$('.notif_copy').show().delay(2000).slideUp();
			},
	        copy:function(){return $('input#dynamic').val();}
	    });
	});
</script>
</html>

