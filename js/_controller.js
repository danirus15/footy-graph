/*S:lazy load image*/
jQuery(document).ready(function() {
    jQuery("img").lazy({
        effect: "fadeIn",
        effectTime: 1000
    });
});

/*s:user*/
$('#user').click(function(){
    if($( "#fm_user" ).hasClass( "active" )){
        $("#drop_users").fadeOut(200);
        $(".fm_user").removeClass("active");
    }
    else {
        $("#drop_users").fadeIn(200);
        $(".fm_user").addClass("active");
    }
});
$('#drop_users').mouseleave(function(){
  $("#drop_users").fadeOut(200);
        $(".fm_user").removeClass("active");
});
/*e:user*/

/*S:MODALBOX*/
$(".box_modal").click(function (){
  if( $('div').attr('id') == 'pop_box_now'){}
  else{ 
    $(this).removeAttr('href');
    var src  = $(this).attr("alt");
    size   = src.split('|');
        url      = size[0],
        width    = '100%',
        height   = '100%'

    $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
    $("body").css('overflow','hidden');
  }
  rescale();
});
$(function() {
  $(".pop_container").wrapInner( "<div id='pop_wrap'></div>" );
  $('#pop_wrap').css('height',$(window).height());
  $('#pop_wrap').css('width',$('#pop_wrap').parent('.pop_container').width());
});
function rescale(){
  var size = {width: $(window).width() , height: $(window).height() }
  $('#pop_wrap').css('height', size.height );
}
$(window).bind("resize", rescale);
$(".box_modal2").click(function (){
  $("#pop_box2").show();
  $("body").css('overflow','hidden');
});
function pop_next(src){
  size   = src.split('|');
    url      = size[0],
    width    = size[1],
    height   = size[2],
    tops   = 'calc(50% - '+ (height/2) +'px)';
    tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';
  $("#framebox").animate({
    height: height,
    width: width,
  },0).attr('src',url).css('top',tops).css('top',tops2);
};
function closepop()
{ 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
$(".pop_next").click(function (){
  var src = $(this).attr("alt");
  parent.pop_next(src);
});
/*S:MODALBOX*/


// S : Kontroler Untuk TABBING
    $("#tab1").idTabs(function(id,list,set){ 
    $("a",set).removeClass("selected") 
    .filter("[href='"+id+"']",set).addClass("selected"); 
    for(i in list) 
      $(list[i]).hide(); 
    $(id).fadeIn(); 
    return false; 
  });

    $("#tab2").idTabs(function(id,list,set){ 
    $("a",set).removeClass("selected") 
    .filter("[href='"+id+"']",set).addClass("selected"); 
    for(i in list) 
      $(list[i]).hide(); 
    $(id).fadeIn(); 
    return false; 
  });

    $("#tab3").idTabs(function(id,list,set){ 
    $("a",set).removeClass("selected") 
    .filter("[href='"+id+"']",set).addClass("selected"); 
    for(i in list) 
      $(list[i]).hide(); 
    $(id).fadeIn(); 
    return false; 
  });

    $("#tab4").idTabs(function(id,list,set){ 
    $("a",set).removeClass("selected") 
    .filter("[href='"+id+"']",set).addClass("selected"); 
    for(i in list) 
      $(list[i]).hide(); 
    $(id).fadeIn(); 
    return false; 
  });

// E : Kontroler Untuk TABBING
$(document).ready(function(){




  $('.close_button').click(function(event) {
    $('input').closeChooser();
  });

  $('.set_color_button').click(function(event) {
    $('input').setColor('#cc3333');
  });

  $('.simple_color').simpleColor({
    boxHeight: 15,
    cellWidth: 15,
    cellHeight: 15
  });

  $('.simple_color_kitchen_sink').simpleColor({
    boxHeight: 15,
    cellWidth: 15,
    cellHeight: 15,
    chooserCSS: { 'border': '1px solid #660033' },
    displayCSS: { 'border': '1px solid red' },
    displayColorCode: false,
    livePreview: true,
    onSelect: function(hex, element) {
      alert("You selected #" + hex + " for input #" + element.attr('class'));
    },
    onCellEnter: function(hex, element) {
      console.log("You just entered #" + hex + " for input #" + element.attr('class'));
    },
    onClose: function(element) {
      alert("color chooser closed for input #" + element.attr('class'));
    }
  });

  $('#formation_show a').click(function(event) {
    var id = $(this).attr("id");
    if( $(this).attr('id') == '1'){
      $('#formation_show a').removeClass('selected');
      $('.tim').css('width','200%');
      $(this).addClass('selected');
      $('#player_container .tim2').hide();
      $('.tim_sub .tim_sub2').hide();
      $('.tim_right').hide();
      $('.vs').hide();
      $('.input_nama_tim2').hide();
      $('.tim_color2').hide();
      $('.input_nama_coach2').hide();
      $('.input_nama_coach').addClass('input_nama_coach_full');
      $('.input_nama_tim').addClass('input_nama_tim_full');
      $('.info_team .fr').hide();
    }
    else{
      $('#formation_show a').removeClass('selected');
      $(this).addClass('selected');
      $('.tim').css('width','100%');
      $('#player_container .tim2').show();
      $('.tim_sub .tim_sub2').show();
      $('.tim_right').show();
      $('.vs').show();
      $('.input_nama_tim2').show();
      $('.tim_color2').show();
      $('.input_nama_coach2').show();
      $('.input_nama_coach').removeClass('input_nama_coach_full');
      $('.input_nama_tim').removeClass('input_nama_tim_full');
      $('.info_team .fr').show();
    }
  });

 
  $('#select_pitch a').click(function(event) {
    var id = $(this).attr("id");
    $('#select_pitch a').removeClass('selected');
    $(this).addClass('selected');
    $('#pitch_pattern').removeClass();
    $('#pitch_pattern').addClass('pitch' + id);
  });


  $('.player').click(function() {
    var id = $(this).attr("id");
    var p = $(this);
    var position = p.position();
    var p_left = (position.left + 5);
    var p_top = (position.top - 50);
    $('.pop_name').hide();
    $('.pn' + id).css('left',p_left).css('top',p_top).show();
  });
  $('.psub').click(function() {
    $('.pop_name').hide();
  });

  $('.player_prev').mouseover(function() {
    var id = $(this).attr("id");
    var p = $(this);
    var position = p.position();
    var p_left = (position.left + 5);
    var p_top = (position.top - 70);
    $('.view_player').hide();

   /* if ($(this).has("#1b")) {
      $('.pn' + id).css('left',"calc(100% - 240px)").css('top',p_top).show();
    }
    else{*/
      $('.pn' + id).css('left',p_left).css('top',p_top).show();
    
    
    
  });
  $('.player_prev').mouseleave(function() {
    $('.view_player').hide();
  });

  $('.select_formation_match').click(function() {
    $('.select_formation_match').addClass('select_formation_match_select');
  });
  $('.select_formation_match_select').click(function() {
    $('.select_formation_match').removeClass('select_formation_match_select');
  });

});


