<html>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/header.php";?>
<div class="container container_main">
	<div class="page_title">
		<span>Project</span>
		<!-- <div class="link">
			<a href="">Active</a>
			<a href="">Archive</a>
		</div> -->
		<a class="btn_create box_modal fr" alt="box_create.php|550|300">+ CREATE PROJECT</a>
	</div>
	<div class="pt20"></div>
	<ul class="list_project">
		<li>
			<div class="icon">
				mj
			</div>
			<div class="title">
				Manchester United
				<span>Juventus</span>
			</div>
			<div class="float">
				<a href="create_formasi.php" class="tooltip box_modal" alt="preview.php" title="View"><img src="img/ico_view.png" alt=""></a>
				<a href="create_formasi.php" class="tooltip" title="Edit"><img src="img/ico_edit.png" alt=""></a>
				<div class="embed">www.footygraph.com/12903923</div>
			</div>
		</li>
		<li>
			<div class="icon" style="background-color:#fe0000;">
				ma
			</div>
			<div class="title">
				Manchester United
			</div>
			<div class="float">
				<a href="create_formasi.php" class="tooltip box_modal" alt="preview.php" title="View"><img src="img/ico_view.png" alt=""></a>
				<a href="create_formasi.php" class="tooltip" title="Edit"><img src="img/ico_edit.png" alt=""></a>
				<div class="embed">www.footygraph.com/12903923</div>
			</div>
		</li>
		<li>
			<div class="icon" style="background-color:#000;">
				JU
			</div>
			<div class="title">
				Juventus
			</div>
			<div class="float">
				<a href="create_formasi.php" class="tooltip box_modal" alt="preview.php" title="View"><img src="img/ico_view.png" alt=""></a>
				<a href="create_formasi.php" class="tooltip" title="Edit"><img src="img/ico_edit.png" alt=""></a>
				<div class="embed">www.footygraph.com/12903923</div>
			</div>
		</li>
		<li>
			<div class="icon">
				RB
			</div>
			<div class="title">
				Real Madrid
				<span>Barcelona</span>
			</div>
			<div class="float">
				<a href="create_formasi.php" class="tooltip box_modal" alt="preview.php" title="View"><img src="img/ico_view.png" alt=""></a>
				<a href="create_formasi.php" class="tooltip" title="Edit"><img src="img/ico_edit.png" alt=""></a>
				<div class="embed">www.footygraph.com/12903923</div>
			</div>
		</li>
	</ul>
</div>
<?php include "includes/js.php";?>
</html>