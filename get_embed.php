<!doctype html>
<html>
<?php include "includes/head.php";?>
<body class="body_pop">
<div class="link_formasi_css">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css'>
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim1">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim2">
</div>
<div class="pd20">
	<span class="close_box_in close_box_style">x</span>
	<div class="formasi embed_setting">
		<form action="get_embed.php" method="post">
		<div class="embed_info">
			<div class="pd10">
				<div class="fl w200">
					<h4>Pitch Position</h4>
					<div class="pitch_pos">
						<label>
							<img src="img/formation_1.png" alt="">
						</label>
						<label>
							<img src="img/formation_2.png" alt="">
						</label>
					</div>
				</div>
				<!-- <div class="fl w200">
					<h4>Add Board</h4>
					<label for="">
						<input type="checkbox"> <span>hide (premium)</span> <span class="l_red">?</span>
					</label>
				</div> -->
				<div class="fl w200">
					<h4>Width</h4>
					<div class="embed_width">
						<label>
							<input type="radio" name="width">
							<span></span>
							<span>100%</span>
							<!-- <a class="box_modal inline pl30 f15 l_red" alt="preview_960.php|1000|610">preview</a> -->
						</label>
						<label>
							<input type="radio" name="width">
							<span></span>
							<span>640px</span>
							<!-- <a class="box_modal inline pl30 f15 l_red" alt="preview_640.php|640|550">preview</a> -->
						</label>
						<label>
							<input type="radio" name="width">
							<span></span>
							<span>460px</span>
							<!-- <a class="box_modal inline pl30 f15 l_red" alt="preview_460.php|460|450">preview</a> -->
						</label>
					</div>
				</div>
				<div class="clearfix pt20"></div>
				<h4>embed code</h4>
				<textarea class="textarea"></textarea>
				<input type="submit" value="Copy Embed" class="btn_save">

			</div>
			
		</div>
		<div class="clearfix"></div>
	</div>
</div>



<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>