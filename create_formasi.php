<!doctype html>
<html>
<?php include "includes/head.php";?>
<body>
<?php include "includes/header.php";?>
<div class="link_formasi_css">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css'>
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim1">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim2">
</div>
<div class="container container_main">
	<div class="page_title page_title_sticky">
		<img src="img/ico_feature.png" alt="" class="t_ico">
		<span class="fl">formation for</span>
		<div class="select_formation_match_for">
            <div class="styled-select">
                 <select>
                   <option selected="selected" value=" ">Select League</option>
                   <option>Liga Inggris</option>, 
                   <option>Liga Italia</option>, 
                   <option>Liga Spanyol</option>
                   <option>Piala Eropa 2016</option>
                   <option>Post Match</option>
                 </select>
            </div>
        </div>
		<a class="btn_create box_modal fr ml10" alt="preview.php|1000|550">Preview & Embed</a>
		<!-- <div class="fr ml10">&nbsp;</div>
		<a class="btn_create box_modal fr mr10" alt="get_embed.php|700|375">EMbed</a> -->
	</div>
	<div class="container2 formasi">
		<form action="get_embed.php" method="post">
		<div class="formasi_area">
			<div class="tim_nama">
				<div class="tim_color"><input value='#cc3333' class="simple_color tim_color" /></div>
				<input type="text" class="input_nama_tim" placeholder="Team Name" id="CityAjax">
				<span class="vs">VS</span>
				<input type="text" class="input_nama_tim input_nama_tim2" placeholder="Team Name">
				<div class="tim_color tim_color2"><input value='#cc3333' class="simple_color tim_color" /></div>
				<div class="clearfix"></div>
				<input type="text" class="input_nama_coach" placeholder="Coach Name">
				<input type="text" class="input_nama_coach input_nama_coach2" placeholder="Coach Name">
				<div class="clearfix"></div>
			</div>
			<div class="info_team">
				<div class="fl w200">
					<div class="styled-select">
		                 <select class="select_for">
		                   <option selected="selected" value=" ">Select Formation</option>
		                    <option value="442">4-4-2</option>
		                    <option value="4411">4-4-1-1</option>
		                    <option value="433">4-3-3</option>
		                    <option value="4132">4-1-3-2</option>
		                    <option value="41212">4-1-2-1-2</option>
		                    <option value="4213">4-2-1-3</option>
		                    <option value="4231">4-2-3-1</option>
		                    <option value="451">4-5-1</option>
		                    <option value="343">3-4-3</option>
		                    <option value="352">3-5-2</option>
		                    <option value="3412">3-4-1-2</option>
		                    <option value="3421">3-4-2-1</option>
		                 </select>
		            </div>
	            </div>
	            
	            <!-- <div class=" select_formation_match">
					<span>Pre Match</span>
					<label class="match_select">
						<span></span>
						<input type="checkbox">
					</label>
					<span>Post Match</span>
				</div> -->
	            <div class="fr w200">
					<div class="styled-select">
		                 <select class="select_for2">
		                   <option selected="selected" value=" ">Select Formation</option>
		                    <option value="442">4-4-2</option>
		                    <option value="4411">4-4-1-1</option>
		                    <option value="433">4-3-3</option>
		                    <option value="4132">4-1-3-2</option>
		                    <option value="41212">4-1-2-1-2</option>
		                    <option value="4213">4-2-1-3</option>
		                    <option value="4231">4-2-3-1</option>
		                    <option value="451">4-5-1</option>
		                    <option value="343">3-4-3</option>
		                    <option value="352">3-5-2</option>
		                    <option value="3412">3-4-1-2</option>
		                    <option value="3421">3-4-2-1</option>
		                 </select>
		            </div>
	            </div>
	            <div class="clearfix"></div>
            </div>
			<div class="pitch">
				<div class="tboard">
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
						<div class="tboard_change">
							<a class="box_modal" alt="box_premium.php|550|300">Change Board</a>
						</div>
					</div>
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
						<div class="tboard_change">
							<a class="box_modal" alt="box_board.php|550|300">Change Board</a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="pitch_left">
					<div class="select_formation" id="formation_show">
						<span>Formation Show:</span>
						<div class="clearfix pt5"></div>
						<a id="1">
							<span>1 team</span>
						</a>
						<a id="2" class="selected">
							<span>2 team</span>
						</a>
					</div>
				</div>
				<div class="pitch_left pitch_right">
					<div class="select_pitch" id="select_pitch">
						<span>Pitch Pattern:</span>
						<div class="clearfix pt5"></div>
						<a class="selected" id="1"><img src="img/pattern1.jpg" alt=""></a>
						<a id="2"><img src="img/pattern2.jpg" alt=""></a>
						<a id="3"><img src="img/pattern3.jpg" alt=""></a>
						<a id="4"><img src="img/pattern4.jpg" alt=""></a>
					</div>
				</div>
				<div class="line" id="player_container">
					<div id="pitch_pattern"></div>
					<div class="tim tim1">
						<div id="tf1">
							<div class="player" id="1a">
								<div class="bullet"><span id="pno1a"></span></div>
								<strong id="pname1a"></strong>

							</div>
							<div class="player" id="2a">
								<div class="bullet"><span id="pno2a"></span></div>
								<strong id="pname2a"></strong>
								<div class="ico">
									<img src="img/ico-yellow.png" alt="">
								</div>
							</div>
							<div class="player" id="3a">
								<div class="bullet"><span id="pno3a"></span></div>
								<strong id="pname3a"></strong>
							</div>
							<div class="player" id="4a">
								<div class="bullet"><span id="pno4a"></span></div>
								<strong id="pname4a"></strong>
							</div>
							<div class="player" id="5a">
								<div class="bullet"><span id="pno5a"></span></div>
								<strong id="pname5a"></strong>
								<div class="ico">
									<img src="img/ico-yellow.png" alt="">
									<img src="img/ico-red.png" alt="">
								</div>
							</div>
							<div class="player" id="6a">
								<div class="bullet"><span id="pno6a"></span></div>
								<strong id="pname6a"></strong>
								<div class="ico">
									<img src="img/ico-goal.png" alt="">
									<img src="img/ico-assist.png" alt="">
								</div>
							</div>
							<div class="player" id="7a">
								<div class="bullet"><span id="pno7a"></span></div>
								<strong id="pname7a"></strong>
							</div>
							<div class="player" id="8a">
								<div class="bullet"><span id="pno8a"></span></div>
								<strong id="pname8a"></strong>
							</div>
							<div class="player" id="9a">
								<div class="bullet"><span id="pno9a"></span></div>
								<strong id="pname9a"></strong>
							</div>
							<div class="player" id="10a">
								<div class="bullet"><span id="pno10a"></span></div>
								<strong id="pname10a"></strong>
							</div>
							<div class="player" id="11a">
								<div class="bullet"><span id="pno11a"></span></div>
								<strong id="pname11a"></strong>
							</div>
						</div>
						<div class="pop_name pn1a">
							<input type="text" class="pno" id="1a" placeholder="No">
							<input type="text" class="pname" id="1a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn2a">
							<input type="text" class="pno" id="2a" placeholder="No">
							<input type="text" class="pname" id="2a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn3a">
							<input type="text" class="pno" id="3a" placeholder="No">
							<input type="text" class="pname" id="3a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn4a">
							<input type="text" class="pno" id="4a" placeholder="No">
							<input type="text" class="pname" id="4a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn5a">
							<input type="text" class="pno" id="5a" placeholder="No">
							<input type="text" class="pname" id="5a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn6a">
							<input type="text" class="pno" id="6a" placeholder="No">
							<input type="text" class="pname" id="6a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn7a">
							<input type="text" class="pno" id="7a" placeholder="No">
							<input type="text" class="pname" id="7a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn8a">
							<input type="text" class="pno" id="8a" placeholder="No">
							<input type="text" class="pname" id="8a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn9a">
							<input type="text" class="pno" id="9a" placeholder="No">
							<input type="text" class="pname" id="9a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn10a">
							<input type="text" class="pno" id="10a" placeholder="No">
							<input type="text" class="pname" id="10a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn11a">
							<input type="text" class="pno" id="11a" placeholder="No">
							<input type="text" class="pname" id="11a" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
					</div>
					<div class="tim tim2">
						<div id="tf2">
							<div class="player" id="1b">
								<div class="bullet"><span id="pno1b"></span></div>
								<strong id="pname1b"></strong>
							</div>
							<div class="player" id="2b">
								<div class="bullet"><span id="pno2b"></span></div>
								<strong id="pname2b"></strong>
							</div>
							<div class="player" id="3b">
								<div class="bullet"><span id="pno3b"></span></div>
								<strong id="pname3b"></strong>
							</div>
							<div class="player" id="4b">
								<div class="bullet"><span id="pno4b"></span></div>
								<strong id="pname4b"></strong>
								<div class="ico">
									<img src="img/ico-goal.png" alt="">
									<img src="img/ico-goal.png" alt="">
								</div>
							</div>
							<div class="player" id="5b">
								<div class="bullet"><span id="pno5b"></span></div>
								<strong id="pname5b"></strong>
							</div>
							<div class="player" id="6b">
								<div class="bullet"><span id="pno6b"></span></div>
								<strong id="pname6b"></strong>
								<div class="ico">
									<img src="img/ico-sub.png" alt="">
									<img src="img/ico-goal.png" alt="">
								</div>
							</div>
							<div class="player" id="7b">
								<div class="bullet"><span id="pno7b"></span></div>
								<strong id="pname7b"></strong>
							</div>
							<div class="player" id="8b">
								<div class="bullet"><span id="pno8b"></span></div>
								<strong id="pname8b"></strong>
							</div>
							<div class="player" id="9b">
								<div class="bullet"><span id="pno9b"></span></div>
								<strong id="pname9b"></strong>
							</div>
							<div class="player" id="10b">
								<div class="bullet"><span id="pno10b"></span></div>
								<strong id="pname10b"></strong>
							</div>
							<div class="player" id="11b">
								<div class="bullet"><span id="pno11b"></span></div>
								<strong id="pname11b"></strong>
							</div>
						</div>
						<div class="pop_name pn1b">
							<input type="text" class="pno" id="1b" placeholder="No">
							<input type="text" class="pname" id="1b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn2b">
							<input type="text" class="pno" id="2b" placeholder="No">
							<input type="text" class="pname" id="2b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn3b">
							<input type="text" class="pno" id="3b" placeholder="No">
							<input type="text" class="pname" id="3b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn4b">
							<input type="text" class="pno" id="4b" placeholder="No">
							<input type="text" class="pname" id="4b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn5b">
							<input type="text" class="pno" id="5b" placeholder="No">
							<input type="text" class="pname" id="5b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn6b">
							<input type="text" class="pno" id="6b" placeholder="No">
							<input type="text" class="pname" id="6b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn7b">
							<input type="text" class="pno" id="7b" placeholder="No">
							<input type="text" class="pname" id="7b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn8b">
							<input type="text" class="pno" id="8b" placeholder="No">
							<input type="text" class="pname" id="8b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn9b">
							<input type="text" class="pno" id="9b" placeholder="No">
							<input type="text" class="pname" id="9b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn10b">
							<input type="text" class="pno" id="10b" placeholder="No">
							<input type="text" class="pname" id="10b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
						<div class="pop_name pn11b">
							<input type="text" class="pno" id="11b" placeholder="No">
							<input type="text" class="pname" id="11b" placeholder="Name">
							<a class="psub">X</a>	
							<img src="img/arrow_down2.png" alt="" class="arrow">
						</div>
					</div>

				</div>
				<div class="clearfix"></div>
				<!-- s:subtitution -->
					<div class="tim_sub">
						<div class="tim_sub1">
							<div class="player" id="12a">
								<div class="bullet"><span id="pno12a">12</span></div>
								<strong id="pname12a">KIPER</strong>
								<div class="ico">
									<img src="img/ico-goal.png" alt="">
									<img src="img/ico-assist.png" alt="">
								</div>
							</div>
							<div class="player" id="13a">
								<div class="bullet"><span id="pno13a"></span></div>
								<strong id="pname13a"></strong>
							</div>
							<div class="player" id="14a">
								<div class="bullet"><span id="pno14a">33</span></div>
								<strong id="pname14a">Pemain</strong>
								<div class="ico">
									<img src="img/ico-sub.png" alt="">
									<img src="img/ico-assist.png" alt="">
								</div>
							</div>
							<div class="player" id="15a">
								<div class="bullet"><span id="pno15a"></span></div>
								<strong id="pname15a"></strong>
							</div>
							<div class="player" id="16a">
								<div class="bullet"><span id="pno16a"></span></div>
								<strong id="pname16a"></strong>
							</div>
							<div class="player" id="17a">
								<div class="bullet"><span id="pno17a"></span></div>
								<strong id="pname16a"></strong>
							</div>
							<div class="player" id="18a">
								<div class="bullet"><span id="pno18a"></span></div>
								<strong id="pname16a"></strong>
							</div>
							<div class="pop_name pn12a">
								<input type="text" class="pno" id="12a" placeholder="No">
								<input type="text" class="pname" id="12a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn13a">
								<input type="text" class="pno" id="13a" placeholder="No">
								<input type="text" class="pname" id="13a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn14a">
								<input type="text" class="pno" id="14a" placeholder="No">
								<input type="text" class="pname" id="14a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn15a">
								<input type="text" class="pno" id="15a" placeholder="No">
								<input type="text" class="pname" id="15a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn16a">
								<input type="text" class="pno" id="16a" placeholder="No">
								<input type="text" class="pname" id="16a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn17a">
								<input type="text" class="pno" id="17a" placeholder="No">
								<input type="text" class="pname" id="17a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn18a">
								<input type="text" class="pno" id="18a" placeholder="No">
								<input type="text" class="pname" id="18a" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
						</div>
						<div class="tim_sub2">
							<div class="player" id="12b">
								<div class="bullet"><span id="pno12b"></span></div>
								<strong id="pname12b"></strong>
							</div>
							<div class="player" id="13b">
								<div class="bullet"><span id="pno13b"></span></div>
								<strong id="pname13b"></strong>
							</div>
							<div class="player" id="14b">
								<div class="bullet"><span id="pno14b"></span></div>
								<strong id="pname14b"></strong>
							</div>
							<div class="player" id="15b">
								<div class="bullet"><span id="pno15b"></span></div>
								<strong id="pname15b"></strong>
							</div>
							<div class="player" id="16b">
								<div class="bullet"><span id="pno16b"></span></div>
								<strong id="pname16b"></strong>
							</div>
							<div class="player" id="17b">
								<div class="bullet"><span id="pno17b"></span></div>
								<strong id="pname16b"></strong>
							</div>
							<div class="player" id="18b">
								<div class="bullet"><span id="pno18b"></span></div>
								<strong id="pname16b"></strong>
							</div>
							<div class="pop_name pn12b">
								<input type="text" class="pno" id="12b" placeholder="No">
								<input type="text" class="pname" id="12b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn13b">
								<input type="text" class="pno" id="13b" placeholder="No">
								<input type="text" class="pname" id="13b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn14b">
								<input type="text" class="pno" id="14b" placeholder="No">
								<input type="text" class="pname" id="14b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn15b">
								<input type="text" class="pno" id="15b" placeholder="No">
								<input type="text" class="pname" id="15b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn16b">
								<input type="text" class="pno" id="16b" placeholder="No">
								<input type="text" class="pname" id="16b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn17b">
								<input type="text" class="pno" id="17b" placeholder="No">
								<input type="text" class="pname" id="17b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
							<div class="pop_name pn18b">
								<input type="text" class="pno" id="18b" placeholder="No">
								<input type="text" class="pname" id="18b" placeholder="Name">
								<a class="psub">X</a>	
								<img src="img/arrow_down2.png" alt="" class="arrow">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- e:subtitution -->
				<!-- <div align="center">
					<input type="submit" value="Save" class="btn_save">
					<input type="submit" value="Create Embed" class="btn_save btn_embed">
				</div> -->
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>



<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>