<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <title>Footy Graph</title>
  <link href="../css/frame.style.css" rel="stylesheet" type="text/css">
  <link href="../css/fonts.css" rel="stylesheet" type="text/css">
  <link href="../css/frame.style.css" rel="stylesheet" type="text/css">
  <link href="css/footygraph.style.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id='header'>
  <div class="nav">
    <a href="#welcome" class="jslide"><img src="images/footy_logo.png" alt="footygraph"></a>
  </div>
  <div class="nav_menu">
    <a href="#about" class="jslide">About Us</a>
    <a href="#feature" class="jslide">Features</a>
    <!-- <a href="#howwork" class="jslide">How it Work</a> -->
    <a href="#join" class="jslide">Join Now</a>
    <a href="login.php">Login</a>
  </div>
   <div class="share">
      <div class="menu_share"><img src="images/ico_share.png" alt=""></div>
      <div class="list_share">
        <a href="#"><img src="images/ico_fb.png" alt="facebook"></a>
        <a href="#"><img src="images/ico_tw.png" alt="twitter"></a>
      </div>
    </div>
    
</div>
<div class="cover">
  <article>
      <div id='header' class="header_cover">
        <div class="nav">
          <a href="#welcome" class="jslide"><img src="images/footy_logo.png" alt="footygraph"></a>
        </div>
        <div class="nav_menu">
          <a href="#join" class="jslide">Join Now</a>
          <a href="login.php">Login</a>
        </div>
      </div>
      <div class="layer_welcome">
        <div align="center">
          <br><br><br><br><br><br><br><br><br><br><br><br>
          <div class="clearfix"></div>
          <div class="f28">
            Free Instant Football Features<br>for Everyone<br>
           <b> No Coding Required</b>
          </div>
          <br>
          <a href="#feature" class="btn_next jslide">
            <span>Find More</span>
          </a>
        </div>
        <div class="share share_wp">
          <a href="#"><img src="images/ico_fb.png" alt="facebook"></a>
          <a href="#"><img src="images/ico_tw.png" alt="twitter"></a>
        </div>
      </div>
      <img src="images/bg_wp.png" alt="" class="wp_cover">
      <!-- <video autoplay="" loop="" poster="images/welcome.jpg" id="videowelcome" muted>
        <source src="images/bgvideo.mp4" type="video/mp4">
      </video> -->
    </article>
</div>
<div class="wp_content">
  <article id="about">
      <div class="container">
        <h1 class="jdl">FOOTYGRAPH IS PERFECT FOR FOOTBALL LOVER </h1>
        Our simple editor can easily help you to create a beautiful features in seconds, no matter what your technical level
        <br><br><br><br><br><br>
        <div class="list_1 list_1-3">
          <img src="images/ico_formasi.png" alt=""><br>
          <h3>FORMATION</h3>
          You can easily create your own formation for pre or post match, and you can directly embed in your site
        </div>
        <div class="list_1 list_1-3">
          <img src="images/ico_head.png" alt=""><br>
          <h3>head to head</h3>
          Create the statistics of the two teams that will compete
        </div>
        <div class="list_1 list_1-3">
          <img src="images/ico_timeline.png" alt=""><br>
          <h3>timeline</h3>
          Facilitate you in presenting the game in every minute just by filling the goals, cards, and substitutions
        </div>
      <div class="clearfix"></div>
      </div>
    </article>

  

    <article id="feature">
      <div class="container">
        <h1 class="jdl">FEATURES</h1>
        Our simple editor can easily help you to create a beautiful features in seconds, no matter what your technical level
        <br><br><br>
        <div class="list_1">
          <img src="images/ico_feature.png" alt=""><br>
          <h3>Fast & beautiful features</h3>
          footygraph makes it super quick and easy a beautiful interactive content
        </div>
        <div class="list_1">
          <img src="images/ico_simple.png" alt=""><br>
          <h3>Simple yet powerful</h3>
          Footygraph are unabashedly easy to use but give you powerful ways to increase engagement on your sites
        </div>
        <div class="list_1">
          <img src="images/ico_mobile.png" alt=""><br>
          <h3>For mobile & much more</h3>
          Footygraph is designed to fit your site — no matter where it’s seen on mobile and desktop
        </div>
        <div class="list_1">
          <img src="images/ico_sosmed.png" alt=""><br>
          <h3>Share through social media</h3>
          footygraph don’t just work on your sites, they’re perfect fit with twitter, Facebook and more
        </div>
      <div class="clearfix"></div>
      </div>
    </article>

    <article id="join">
      <div class="container">
        <br><br><br><br><br><br>
        <h1 class="jdl">SIMPLE FOR EVERYONE</h1>
        Get unlimited project free. Join us to get started
        <br><br><br>
        <a href="join.php" class="joinnow">JOIN NOW</a>
      <div class="clearfix"></div>
      </div>
    </article>
</div>

<script src='js/js.js'></script>
<script src='js/imgLiquid-min.js'></script>
<script src='js/controller.js'></script>
</body>
</html>
