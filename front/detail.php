<!DOCTYPE html>
<?php $baseurl = "http://localhost/design/d2014/detikcom";?>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <title>20 Wolipop - Detail</title>
  <link rel="stylesheet" href="css/style.css">
</head>
<script src='js/js.js'></script>

<body>
<div id='header' class="header_detail">
  <div class="nav">
    <a href="index.php" class="back"><img src="images/nav_back.png" alt=""></a>
    <a href="#"><img src="images/logo.png" alt=""></a>
  </div>
   <div class="share">
      <div class="menu_share"><img src="images/ico_share.png" alt=""></div>
      <div class="list_share">
        <a href="#"><img src="images/ico_fb.png" alt=""></a>
        <a href="#"><img src="images/ico_tw.png" alt=""></a>
        <a href="#"><img src="images/ico_gplus.png" alt=""></a>
        <a href="#"><img src="images/ico_p.png" alt=""></a>
      </div>
    </div>
</div>
<span class="close_box_in">x</span>
<div class="detail">
  <div class="detail_head">
   <div class="date">Selasa, 18/11/2014 11:20 WIB</div>
    <div class="subjdl">Intimate Interview</div>
    <div class="jdl">Cerita Fatimah Habsyi yang Pernah Rias Henna di Kepala Ahmad Dhani</div>
    <div class="author">Arina Yulistara - wolipop</div>
  </div>
  <div class="detail_kiri">
    <div class="pic_detail">
      <img src="images/wo4.jpg" alt="">
      <span>Dok. Instagram Fatimah Habsyi</span>
    </div>
    <div class="pic_detail pic_detail2">
      <img src="images/wo3.jpg" alt="">
      <span>Dok. Instagram Fatimah Habsyi</span>
    </div>
    <div class="detail_text">
      Jakarta - Fatimah Habsyi dikenal sebagai perias henna profesional yang kliennya tidak hanya berasal dari Indonesia tapi juga luar negeri seperti Singapura, Brunei Darussalam, hingga Nepal. Wanita yang kerap disapa Ima itu juga menjadi langganan pejabat serta aktris Tanah Air baik untuk acara spesial maupun pernikahan.
<br><br>
Ketika berbincang dengan Wolipop beberapa waktu lalu, Ima tidak hanya berbagi cerita mengenai awal mula dirinya terjun menjadi perias henna. Wanita 23 tahun itu juga sedikit berkisah tentang pekerjaannya yang bisa bertemu banyak orang penting seperti anak pejabat serta selebriti. 
<br><br>
Salah satu pengalaman yang tak terlupakan baginya ketika ia melukis henna di atas kepala musisi populer Indonesia, Ahmad Dhani. Bagaimana hal itu bisa terjadi? Ima mulai bercerita, di 2010 lalu Dhani sedang mengerjakan video klip untuk Mulan Jameela. Di dalam video klip tersebut, Mulan akan tampil dengan riasan henna di tangan serta kakinya. 
<br><br>
<img src="images/wo4.jpg" alt="">
<br><br>
Wanita <a href="index.php">yang hobi</a> memasak ini kemudian diminta datang ke rumah Dhani untuk membuatkan riasan henna. Awalnya Ima tidak tahu kalau yang memesan jasanya itu adalah pihak manajemen dari musisi 42 tahun itu.
<br><br>
"Kan kalau orang booking biasanya lewat telepon, aku nggak tahu awalnya nggak tahunya rumah Ahmad Dhani ternyata dia booking untuk video klipnya Mulan Jameela," ujar Ima saat berbincang dengan Wolipop di kawasan Tebet, Jakarta Selatan.
    <br><br>
    <ul>
      <li>Tes 1</li>
      <li>Tes 2</li>
      <li>Tes 3</li>
      <li>Tes 4</li>
    </ul>
    <br>
    <ol>
      <li>Tes 1</li>
      <li>Tes 2</li>
      <li>Tes 3</li>
      <li>Tes 4</li>
    </ol>
    <br>
    tabel:
    <table>
      <tr>
        <td>1</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
      </tr>
      <tr>
        <td>2</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
      </tr>
      <tr>
        <td>3</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
        <td>asdfasf</td>
      </tr>
    </table>
    <div class="newstag newstag_detail">
    Newstag menolak penyelenggaraan <a href="#">Ajang Miss World 2013 di Pulau Bali</a>.
  </div>
    <div class="clearfix pt20"></div>
    <div class="articleshare_new">
      <a href="#.php" class="tw"><img src="images/s_icon_tw.png"><span>3000</span></a>
      <a href="#.php" class="gplus"><img src="images/s_icon_g+.png"><span>30</span></a>
      <a href="#.php" class="fb"><img src="images/s_icon_fb.png"><span>30</span></a>
      <a href="#.php"><img src="images/s_icon_ms.png"><span>30</span></a>
    </div>
    <div class="clearfix pt20"></div>
    <div id="bacajugabox">
        <div class="title_new"><strong>Artikel Terkait</strong></div>
        <div class="list_bc">
            <a href="#">
                <span class="pic"><img src="http://images.detik.com/content/2014/08/08/431/5-chris-dalem.jpg" alt=""></span>
                Taylor Swift Pamer Perut Langsing di Teen Choice Awards 2014
            </a>
            <a href="#">
                <span class="pic"><img src="http://images.detik.com/customthumb/2014/08/08/431/kelchaCOVER.jpg?w=180" alt=""></span>
                Ini Alasan Kate Upton Tak Mau Difoto Telanjang Ini Alasan Kate Upton Tak Mau Difoto Telanjang
            </a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="box_upclose box_upclose2">
        <div class="title">Up Close & Personal</div>
        <div class="list_buc">
          <a href="#">
            <span class="pic"><img src="images/wo1.jpg" alt=""></span>
            <h3>Taylor Swift</h3>
            Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS.
            <div class="clearfix"></div>
          </a>
          <a href="#">
            <span class="pic"><img src="images/wo2.jpg" alt=""></span>
            <h3>Taylor Swift</h3>
            Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS. 
            <div class="clearfix"></div>
          </a>
          <a href="#">
            <span class="pic"><img src="images/wo3.jpg" alt=""></span>
            <h3>Taylor Swift</h3>
            Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS. 
            <div class="clearfix"></div>
          </a>
        </div>
        <a href="#" class="btn_more">More</a>
      </div>
      <div class="komentar_desktop">
        <?php //include "../../komentar/komentar.php"; ?>
      </div>
      <div class="komentar_msite">
        <?php //include "../../../mdetik/komentar/komentar.php"; ?>
      </div>
    </div>
  </div>
  <div class="detail_kanan">
    <div class="banner_reg">
      <a href="#"><img src="images/banner.gif" alt=""></a>
    </div>
    <div class="banner_reg">
      <a href="#"><img src="images/banner.gif" alt=""></a>
    </div>
    <div class="box_upclose">
      <div class="title">Up Close & Personal</div>
      <div class="list_buc">
        <a href="#">
          <span class="pic"><img src="images/wo1.jpg" alt=""></span>
          <h3>Taylor Swift</h3>
          Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS.
          <div class="clearfix"></div>
        </a>
        <a href="#">
          <span class="pic"><img src="images/wo2.jpg" alt=""></span>
          <h3>Taylor Swift</h3>
          Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS. 
          <div class="clearfix"></div>
        </a>
        <a href="#">
          <span class="pic"><img src="images/wo3.jpg" alt=""></span>
          <h3>Taylor Swift</h3>
          Ia memulai kariernya di dunia hiburan pada tahun 1996, sebagai juara pertama di ajang GADIS Sampul majalah GADIS. 
          <div class="clearfix"></div>
        </a>
      </div>
      <a href="#" class="btn_more">More</a>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

<script src='js/imgLiquid-min.js'></script>
<script src='js/controller.js'></script>

</body>

</html>
