<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <title>Footy Graph</title>
  <link href="../css/frame.style.css" rel="stylesheet" type="text/css">
  <link href="../css/fonts.css" rel="stylesheet" type="text/css">
  <link href="../css/frame.style.css" rel="stylesheet" type="text/css">
  <link href="css/footygraph.style.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id='header'>
  <div class="nav">
    <a href="#welcome" class="jslide"><img src="images/logo.png" alt="footygraph"></a>
  </div>
  <div class="nav_menu">
    <a href="index.php">About Us</a>
    <a href="index.php">Features</a>
    <a href="index.php">How To</a>
    <a href="join.php">Join Now</a>
    <a href="login.php">Login</a>
  </div>
   <div class="share">
      <div class="menu_share"><img src="images/ico_share.png" alt=""></div>
      <div class="list_share">
        <a href="#"><img src="images/ico_fb.png" alt="facebook"></a>
        <a href="#"><img src="images/ico_tw.png" alt="twitter"></a>
        <a href="#"><img src="images/ico_gplus.png" alt="google plus"></a>
      </div>
    </div>
</div>

<div class="container">
  <form action="../dashboard.php" method="post" class="form_login form">
    <br><br><br>
    <strong>Full Name</strong>
    <input type="text" class="input">
    <br>
    <strong>Username</strong>
    <input type="text" class="input">
    <br>
    <strong>email</strong>
    <input type="text" class="input">
    <br>
    <strong>password</strong>
    <input type="password" class="input">
    <br>
     <strong>re-password</strong>
    <input type="password" class="input">
    <br>
    <input type="submit" value="Login" class="btn">
    <br><br>
    <a href="login.php">Already have an account? Sign in.</a>
    <br><br><br>
  </form>
</div>

<script src='js/js.js'></script>
<script src='js/imgLiquid-min.js'></script>
<script src='js/controller.js'></script>
</body>
</html>
