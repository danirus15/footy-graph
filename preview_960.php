<html>
<?php include "includes/head.php";?>
<div class="link_formasi_css">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css'>
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim1">
	<link href='css/formation/442_default.css' rel='stylesheet' type='text/css' class="for_tim2">
</div>
<body class="body_pop">
	<span class="close_box_in close_box_style">x</span>
	<div class="pd20">
		<div class="clearfix pt30"></div>
		<div class="p960">
			<div class="formasi_area ">
			<div class="pitch">
				<div class="tboard">
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
					</div>
					<div class="tboard1">
						<a href="#"><img src="img/tboard.png" alt=""></a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="line" id="player_container">
					<div id="pitch_pattern" class="pitch2"></div>
					<div class="tim tim1">
						<div id="tf1">
							<div class="player" id="1a">
								<div class="bullet"><span id="pno1a">1</span></div>
								<strong id="pname1a">De Gea</strong>
							</div>
							<div class="player" id="2a">
								<div class="bullet"><span id="pno2a">3</span></div>
								<strong id="pname2a">shaw</strong>
							</div>
							<div class="player" id="3a">
								<div class="bullet"><span id="pno3a">4</span></div>
								<strong id="pname3a">jones</strong>
							</div>
							<div class="player" id="4a">
								<div class="bullet"><span id="pno4a">5</span></div>
								<strong id="pname4a">rojo</strong>
							</div>
							<div class="player" id="5a">
								<div class="bullet"><span id="pno5a">2</span></div>
								<strong id="pname5a">rafael</strong>
							</div>
							<div class="player" id="6a">
								<div class="bullet"><span id="pno6a">7</span></div>
								<strong id="pname6a">di maria</strong>
							</div>
							<div class="player" id="7a">
								<div class="bullet"><span id="pno7a">17</span></div>
								<strong id="pname7a">blind</strong>
							</div>
							<div class="player" id="8a">
								<div class="bullet"><span id="pno8a">10</span></div>
								<strong id="pname8a">rooney</strong>
							</div>
							<div class="player" id="9a">
								<div class="bullet"><span id="pno9a">8</span></div>
								<strong id="pname9a">mata</strong>
							</div>
							<div class="player" id="10a">
								<div class="bullet"><span id="pno10a">20</span></div>
								<strong id="pname10a">van persie</strong>
							</div>
							<div class="player" id="11a">
								<div class="bullet"><span id="pno11a">9</span></div>
								<strong id="pname11a">falcao</strong>
							</div>
						</div>
					</div>
					<div class="tim tim2">
						<div id="tf2">
							<div class="player" id="1b">
								<div class="bullet"><span id="pno1b">13</span></div>
								<strong id="pname1b">cortouis</strong>
							</div>
							<div class="player" id="2b">
								<div class="bullet"><span id="pno2b">2</span></div>
								<strong id="pname2b">ivanovic</strong>
							</div>
							<div class="player" id="3b">
								<div class="bullet"><span id="pno3b">24</span></div>
								<strong id="pname3b">cahhil</strong>
							</div>
							<div class="player" id="4b">
								<div class="bullet"><span id="pno4b">26</span></div>
								<strong id="pname4b">terry</strong>
							</div>
							<div class="player" id="5b">
								<div class="bullet"><span id="pno5b">3</span></div>
								<strong id="pname5b">luis</strong>
							</div>
							<div class="player" id="6b">
								<div class="bullet"><span id="pno6b">10</span></div>
								<strong id="pname6b">hazard</strong>
							</div>
							<div class="player" id="7b">
								<div class="bullet"><span id="pno7b">8</span></div>
								<strong id="pname7b">oscar</strong>
							</div>
							<div class="player" id="8b">
								<div class="bullet"><span id="pno8b">4</span></div>
								<strong id="pname8b">fabregas</strong>
							</div>
							<div class="player" id="9b">
								<div class="bullet"><span id="pno9b">7</span></div>
								<strong id="pname9b">ramires</strong>
							</div>
							<div class="player" id="10b">
								<div class="bullet"><span id="pno10b">19</span></div>
								<strong id="pname10b">costa</strong>
							</div>
							<div class="player" id="11b">
								<div class="bullet"><span id="pno11b">11</span></div>
								<strong id="pname11b">drogba</strong>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix pt10"></div>
				<div class="fl w50p">
					<h4>4-4-2</h4>
					<h2>Manchester United</h2>
					<h5>Louis van gaal</h5>
				</div>
				<div class="fr w50p" align="right">
					<h4>4-4-2</h4>
					<h2>Chelsea fc</h2>
					<h5>jose mourinho</h5>
				</div>
				<div class="clearfix"></div>
				</form>
			</div>
		</div>
		</div>
	</div>
<?php include "includes/js.php";?>
</html>