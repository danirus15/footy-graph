<?php

$q = strtolower($_GET["q"]);
if (!$q) return;
$items = array(
	"wayne rooney",
	"alexandro del piero",
	"juan mata",
	"andrea pirlo",
	"robin van persie",
	"falcao",
	"alex song",
	"carlos tevez",
	"david de gea",
	"angel di maria",
	"christiano ronaldo",
	"leonel messi",
	"neymar",
	"xavi",
	"xabi alonso",
	"rooben",
	"rojo",
	"bale",
	"benzema"
);

foreach ($items as $key) {
	if (strpos(strtolower($key), $q) !== false) {
		echo "$key\n";
	}
}

?>