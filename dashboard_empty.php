<html>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/header.php";?>
<div class="container container_main">
	<div class="page_title">
		<span>Project</span>
		<!-- <div class="link">
			<a href="">Active</a>
			<a href="">Archive</a>
		</div> -->
		<a class="btn_create box_modal fr" alt="box_create.php|550|300">+ CREATE PROJECT</a>
	</div>
	<div class="pt20"></div>
	<div class="notif_start">
		No project created yet. click the create project button above to create your first project
		<br><br>
		<a class="btn_create box_modal" alt="box_create.php|550|300">+ CREATE PROJECT NOW</a>
	</div>
</div>
<?php include "includes/js.php";?>
</html>