<?php

$q = strtolower($_GET["q"]);
if (!$q) return;
$items = array(
	"sir alex ferguson",
	"conte",
	"jose mourinho",
	"allegri",
	"louis van gaal",
	"arsene wenger",
	"anceloti",
	"inzaghi"
);

foreach ($items as $key) {
	if (strpos(strtolower($key), $q) !== false) {
		echo "$key\n";
	}
}

?>