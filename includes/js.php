<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/plugin.js?123"></script>
<script type="text/javascript" src="js/jquery.simple-color.min.js"></script>
<script type="text/javascript" src="js/_controller.js?123"></script>
<script type="text/javascript" src="js/draggabilly.pkgd.js"></script>
<script>
	/*window.onload = function() {
	  var elems = document.querySelectorAll('.tim .player');

	  function handleDraggerEvent( instance, event, pointer ) {
	    console.log( event.type, instance.position.x, instance.position.y );
	  }
	  for ( var i=0, len = elems.length; i < len; i++ ) {
	    dragger = new Draggabilly( elems[i], {
	      containment: true
	    });
	    dragger.on( 'dragStart', handleDraggerEvent );
	    dragger.on( 'dragEnd', handleDraggerEvent );
	   
	  }

	};*/

	$(function(){
		// while you are typing using keypress event
		$('.pno').bind('keypress',function(){
			// get the value of the input
			var inputValue = $(this).val();
			var inputID = $(this).attr('id');
			//insert the value to label
			$('#pno'+inputID).text(inputValue);
		})
		$(".pname").keypress(function(){
			// get the value of the input
			var inputValue = $(this).val();
			var inputID = $(this).attr('id');
			//insert the value to label
			$('#pname'+inputID).text(inputValue);
		})
	})	
</script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script typet="text/javascript">
	function findValue(li) {
	    if( li == null ) return alert("No match!");

	    // if coming from an AJAX call, let's use the CityId as the value
	    if( !!li.extra ) var sValue = li.extra[0];

	    // otherwise, let's just display the value in the text box
	    else var sValue = li.selectValue;

	    //alert("The value you selected was: " + sValue);
	  }

	  function selectItem(li) {
	        findValue(li);
	  }

	  function formatItem(row) {
	        return row[0];
	  }
	  
	  
	$(".input_nama_tim").autocomplete(
	  "autocomplete_team.php",
	  {
	        delay:10,
	        minChars:2,
	        matchSubset:1,
	        matchContains:1,
	        cacheLength:10,
	        onItemSelect:selectItem,
	        onFindValue:findValue,
	        formatItem:formatItem,
	        autoFill:true
	    }
	);

	/*$(".input_nama_coach").autocomplete(
	  "autocomplete_coach.php",
	  {
	        delay:10,
	        minChars:2,
	        matchSubset:1,
	        matchContains:1,
	        cacheLength:10,
	        onItemSelect:selectItem,
	        onFindValue:findValue,
	        formatItem:formatItem,
	        autoFill:true
	    }
	);
	$(".pname").autocomplete(
	  "autocomplete_player.php",
	  {
	        delay:10,
	        minChars:2,
	        matchSubset:1,
	        matchContains:1,
	        cacheLength:10,
	        onItemSelect:selectItem,
	        onFindValue:findValue,
	        formatItem:formatItem,
	        autoFill:true
	    }
	);*/

	$(document).ready( function ()
	{
	  /* we are assigning change event handler for select box */
		/* it will run when selectbox options are changed */
		$('.select_for').change(function()
		{
			/* setting currently changed option value to option variable */
			var option = $(this).find('option:selected').val();
			var css = "css/formation/"+ option +".css";
			$(".link_formasi_css .for_tim1").attr('href',css);
			$(".pitch #tf1").removeClass();
			$(".pitch #tf1").addClass("f"+ option);
		});
		$('.select_for2').change(function()
		{
			/* setting currently changed option value to option variable */
			var option = $(this).find('option:selected').val();
			var css = "css/formation/"+ option +".css";
			$(".link_formasi_css .for_tim2").attr('href',css);
			$(".pitch #tf2").removeClass();
			$(".pitch #tf2").addClass("f"+ option);
		});
	});
</script>

<script type="text/javascript" src="js/imgLiquid-min.js"></script>
<script>
	$(document).ready(function() {
	    $(".lqd").imgLiquid();
	});
</script>


